<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $fechaNacimiento
 *
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaNacimiento'], 'string','message'=>'La fecha de nacimiento no es valida'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'email'],
            [['nombre','email','fechaNacimiento'],'required','message'=>'Es necesario rellenarlo']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Codigo del Autor',
            'nombre' => 'Nombre completo del Autor',
            'email' => 'Correo electronico',
            'fechaNacimiento' => 'Fecha de Nacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['autor' => 'id']);
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:d/m/Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        //$this->fechaNacimiento=$this->fechaNacimiento=Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:Y/m/d');
        // poder introducir la fecha en castellano
        $this->fechaNacimiento= \DateTime::createFromFormat("d/m/Y", $this->fechaNacimiento)->format("Y/m/d");
        return true;
        
    }
}
