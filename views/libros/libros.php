<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros del autor ' . $autor->nombre;
$atras=Yii::$app->request->referrer;
$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => $atras];
//$this->params['breadcrumbs'][]=['label'=>'Autores','url'=>['autores/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= $this->render('//autores/_view',[
        'model'=>$autor,
    ])?>
   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'titulo',
            'sinopsis',
            'fecha',
            'autor',
        ],
    ]); ?>


</div>

