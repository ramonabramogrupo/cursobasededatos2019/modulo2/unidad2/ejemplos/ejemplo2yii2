<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
?>
<div class="autores-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'email:email',
            'fechaNacimiento',
            'imagen',
             [
              'label'=>'foto del autor',
              'format'=>'raw',
              'value'=>function($model){
                return Html::img('@web/imgs/' . $model->imagen, ['class' => 'img-thumbnail','width'=>'100px']);
              }
            ],
        ],
    ]) ?>

</div>
