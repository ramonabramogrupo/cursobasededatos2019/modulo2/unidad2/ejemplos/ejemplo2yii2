﻿DROP DATABASE IF EXISTS ejemplo2Yii;
CREATE DATABASE ejemplo2Yii;
USE ejemplo2Yii;

CREATE OR REPLACE TABLE libros(
  id int AUTO_INCREMENT,
  titulo varchar(200),
  sinopsis varchar(200),
  fecha date,
  autor int,
  PRIMARY KEY (id)
  );

CREATE OR REPLACE TABLE autores(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  email varchar(50),
  fechaNacimiento date,
  PRIMARY KEY(id)
  );

ALTER TABLE libros 
  ADD CONSTRAINT fklibrosautores 
  FOREIGN KEY (autor) REFERENCES autores(id)
  ON DELETE CASCADE ON UPDATE CASCADE; 
